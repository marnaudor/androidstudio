package fr.acos.myrecyclerlist

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.view.ViewGroup

class EtudiantAdapter(var etudiantList: ArrayList<String>):
 RecyclerView.Adapter<EtudiantAdapter.EtudiantViewHolder>(){
    class EtudiantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val etudiantTextView: TextView = itemView.findViewById(R.id.etudiant_View)

        fun bind(word: String) {
            etudiantTextView.text = word
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EtudiantViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.etudiant_item, parent, false)

        return EtudiantViewHolder(view)
    }

    override fun getItemCount(): Int {
        return etudiantList.size
    }

    override fun onBindViewHolder(holder: EtudiantViewHolder, position: Int) {
        holder.bind(etudiantList[position])
    }

}