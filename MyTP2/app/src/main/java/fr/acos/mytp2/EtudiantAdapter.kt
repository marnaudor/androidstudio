package fr.acos.mytp2

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.view.ViewGroup
import android.widget.ImageView
import fr.acos.mytp2.bo.Student

class EtudiantAdapter(var etudiantList: ArrayList<Student>):
 RecyclerView.Adapter<EtudiantAdapter.EtudiantViewHolder>(){
    class EtudiantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val etudiantTextView: TextView = itemView.findViewById(R.id.etudiant_text)
        private val etudiantImageView: ImageView=itemView.findViewById(R.id.imageView)
        fun bind(word: String,image:Int) {
            etudiantTextView.text = word
            etudiantImageView.id=image
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EtudiantViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.student_item, parent, false)

        return EtudiantViewHolder(view)
    }

    override fun getItemCount(): Int {
        return etudiantList.size
    }

    override fun onBindViewHolder(holder: EtudiantViewHolder, position: Int) {
        holder.bind(etudiantList[position].mnom+"  "+etudiantList[position].mprenom,R.drawable.manager)
       // with(holder) { bind("text") }
    }

}