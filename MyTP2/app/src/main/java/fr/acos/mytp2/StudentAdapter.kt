package fr.acos.mytp2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.acos.mytp2.bo.Student

class StudentAdapter(var etudiantList: ArrayList<String>):
  RecyclerView.Adapter<StudentAdapter.StudentViewHolder>(){

      class StudentViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
          private val etudiantTextView:TextView=itemView.findViewById(R.id.etudiant_text)

          fun bind (word: String){
              etudiantTextView.text=word
          }
      }

    override fun onCreateViewHolder(parent:ViewGroup,viewType:Int):StudentViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.student_item,parent,false)
        return StudentViewHolder(view)
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
        return etudiantList.size
    }

    override fun onBindViewHolder(holder:StudentViewHolder,position :Int){
        holder.bind(etudiantList[position])
    }
}