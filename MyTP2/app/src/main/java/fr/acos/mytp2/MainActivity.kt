package fr.acos.mytp2

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import fr.acos.mytp2.bo.Student

class MainActivity : AppCompatActivity()
    /*,AdapterView.OnItemSelectedListener*/ {


    lateinit var myspinner: Spinner //by lazy { findViewById(R.id.spinner) }
   var matieres = mutableListOf<String>("Cours","TP","")
    val etudiant1= Student("Ar","Mi","ma")
    var etudiant= mutableListOf<Student>(etudiant1)
    lateinit var listView: ListView
    var arrayList: ArrayList<Student> = ArrayList()
    var arrayList1:ArrayList<String> = ArrayList()
    lateinit var recyclerView: RecyclerView
    lateinit var imageView: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
       super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView=findViewById(R.id.etudiant)
  //      imageView=findViewById(R.id.imageView)
        arrayList.add(Student("Arnaud","Michel","Masculin"))
        arrayList.add(Student("Arnoux","Miguel","Masculin"))
        arrayList.add(Student("Arna","Michele","Feminin"))
        arrayList1.add("Michel")
        arrayList1.add("Arnaud")
        val etudiantList=this.resources.getStringArray(R.array.Matieres)
        recyclerView.adapter=EtudiantAdapter(arrayList)

        myspinner=findViewById(R.id.spinner)
      //  listView=findViewById(R.id.etudiant)
      //  val adapte_e=ArrayAdapter<Student>(this,android.R.layout.simple_list_item_1,etudiant)
      //  listView.adapter=adapte_e

        val adapte=ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,matieres)
        myspinner!!.adapter=adapte
      //  myspinner.onItemSelectedListener=this@MainActivity
   with(myspinner)
      {
          adapter = adapte
          setSelection(2, false)
         // onItemSelectedListener = this@MainActivity
          prompt = "Select your favourite language"
          gravity = Gravity.CENTER

      }
        myspinner.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
                showToast(this@MainActivity," Spinner  :}",1000)

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position==0)
                    showToast(this@MainActivity," Spinner  :${position} et ${matieres[position]}",1000)
                //return
                if (position==1){
                    showToast(this@MainActivity," Spinner  :${position} et ${matieres[position]}",1000)
                    //return
                }
                return
               // TODO("Not yet implemented")
             }
        }
       // val adaptestudent=ArrayAdapter<Student>(this,android.R.layout.simple_list_item_1,student)

    }

/*

*/
    private fun showToast(context: Context= applicationContext, message: String, duree: Int=Toast.LENGTH_LONG) {
        Toast.makeText(context, message, duree).show()
    }


}